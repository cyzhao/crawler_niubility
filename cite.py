from urllib import request as ureq
import requests
from lxml import html
import gzip
import re,math
import time,random




def get_author(one_list):

    c=[]
    b = [item for line in one_list for item in line.split(',')]
    for a_i in b:
        if '\xa0' in a_i:
            a_i = a_i[:a_i.find(u'\xa0')]
            
        res = re.search(r'\w\s\w+', a_i)
        if res != None:
            c.append(res.group(0))
    return c


def get_citation(query_url):

    header_dict={'Host': 'xs.glgoo.top',
                 'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Mobile Safari/537.36',
                 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                 'Accept-Language': 'zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3',
                 'Referer': 'https://xs.glgoo.top/schhp?hl=zh-CN',
                 'Connection': 'keep-alive',
                 'cookie':'CONSENT=YES+CN.zh-CN+20180318-09-1; GSP=LM=1533191995:S=vJmdnVriU4rPvp3a; HSID=Aoc6Se-3hEkAAnwkr; SSID=A-a1jEsNw-_3hE2c6; APISID=u4MChJbypLJFHJIj/ASCtQFk8bySjYzRhg; SAPISID=yoqKuGskdFmgQMBN/AmleMiEYN52GcUYv4; SID=zQYRThMe6eQZRVfNMIhLvfLKv78shc3K77oVc2ENTLAIoF5LUwkHz4sgH4IyFntj9NZG1Q.; OGPC=19007661-2:19008186-1:19008076-2:19008374-2:19008966-1:19009193-2:577841152-2:19009353-2:19009950-2:; NID=152=s1lStBnIRtqBYHkee7-6LpGz99iWYijVW07FPltVhgE8hkqlKS04XUW7C1lXuTDep5Kf7OdaM2vR7oI6T3Tc5b_rON0g8A7ZQ1UCfBMPB2GMc15ZN-cjjezweqBSHwbycEmRUSQy566-weXI3oVb01vlKdtE216xrI4qFuNJ2SMzwIhPfNMqHy9zCLJcAKMtLpGpl94qi13CS02VYK0AsuZZX6AUqKNtWuJ7GLZWOFSS; 1P_JAR=2019-1-2-10; SIDCC=ABtHo-HUgz4V9IJJUFa6PDfVglW6t4vrsGjGS1Pn8Q3ElLMNPaqGxYcHmQIWzl8BhKBTDtWDCqo'}


    
    

    print("query_url is : ", query_url)
    re_se = requests.Session()
    # re_se.proxies = proxy
    page=re_se.get(query_url,headers=header_dict)
    print("read successfully")
    tree = html.fromstring(page.text)


    citation_text = tree.xpath("//a[contains(text(), 'Cited by')]/text()")
    try:
        citation_num_all = int(citation_text[0].split()[-1])
        citation_papers_url = tree.xpath("//a[contains(text(), 'Cited by')]/@href")
        print("citation num and url", citation_num_all, citation_papers_url)
    except:
        citation_num_all = 0
        citation_papers_url = ['']

    
    authors = tree.xpath('//div[@id="gs_res_ccl_mid"][1]/div[1]/div[@class="gs_ri"]/div[@class="gs_a"]//a/text()')
    print("authors with url is  ", authors)
    auth_to_add = tree.xpath('//div[@id="gs_res_ccl_mid"][1]/div[1]/div[@class="gs_ri"]/div[@class="gs_a"]/text()')
    authors += get_author(auth_to_add)
    
    

            
    print(authors)
    return citation_num_all,citation_papers_url[0], authors

def select_self_cite(num, url, authors):
    self_cite_num = 0
    header_dict={'Host': 'xs.glgoo.top',
                 'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Mobile Safari/537.36',
                 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                 'Accept-Language': 'zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3',
                 'Referer': 'https://xs.glgoo.top/schhp?hl=zh-CN',
                 'Connection': 'keep-alive',
                 'cookie':'CONSENT=YES+CN.zh-CN+20180318-09-1; GSP=LM=1533191995:S=vJmdnVriU4rPvp3a; HSID=Aoc6Se-3hEkAAnwkr; SSID=A-a1jEsNw-_3hE2c6; APISID=u4MChJbypLJFHJIj/ASCtQFk8bySjYzRhg; SAPISID=yoqKuGskdFmgQMBN/AmleMiEYN52GcUYv4; SID=zQYRThMe6eQZRVfNMIhLvfLKv78shc3K77oVc2ENTLAIoF5LUwkHz4sgH4IyFntj9NZG1Q.; OGPC=19007661-2:19008186-1:19008076-2:19008374-2:19008966-1:19009193-2:577841152-2:19009353-2:19009950-2:; NID=152=s1lStBnIRtqBYHkee7-6LpGz99iWYijVW07FPltVhgE8hkqlKS04XUW7C1lXuTDep5Kf7OdaM2vR7oI6T3Tc5b_rON0g8A7ZQ1UCfBMPB2GMc15ZN-cjjezweqBSHwbycEmRUSQy566-weXI3oVb01vlKdtE216xrI4qFuNJ2SMzwIhPfNMqHy9zCLJcAKMtLpGpl94qi13CS02VYK0AsuZZX6AUqKNtWuJ7GLZWOFSS; 1P_JAR=2019-1-2-10; SIDCC=ABtHo-HUgz4V9IJJUFa6PDfVglW6t4vrsGjGS1Pn8Q3ElLMNPaqGxYcHmQIWzl8BhKBTDtWDCqo'}
    
    url = url.split('?')[1]

    pages = math.ceil(num/10.0)
    for page_id in range(pages):
        # time.sleep(random.uniform(3,10))
        url = 'https://xs.glgoo.top/scholar?start=' + str(page_id*10) + '&' +  url
        print("cites urls: ", url)
        page = requests.Session().get(url,headers=header_dict,allow_redirects=False)
        print("read successfully")
        tree = html.fromstring(page.text)

        for i in range(10):
            
            cite_authors = tree.xpath('//div[@id="gs_res_ccl_mid"][1]/div[' + str(i) + ']/div[@class="gs_ri"]/div[@class="gs_a"]//a/text()')
            print('cite_authors with url ', cite_authors)
            cite_auth_to_add = tree.xpath('//div[@id="gs_res_ccl_mid"][1]/div[' + str(i) + ']/div[@class="gs_ri"]/div[@class="gs_a"]/text()')
            cite_authors += get_author(cite_auth_to_add)

            #check if has same author
            for auth in authors:
                break_auth = False
                for cite_auth in cite_authors:
                    if auth == cite_auth:
                        self_cite_num += 1
                        print("page = ", str(page_id), " item = ", str(i), " ", auth)
                        break_auth = True
                        break
                if break_auth:
                    break
    print(self_cite_num)
    return self_cite_num                
    
if __name__ == '__main__':

    with open('papers.txt','r') as f:
        papers = f.readlines()
        
    papers_name = []
    for paper in papers:
        strings = paper.replace('，', ',').split(',')
        strings.reverse()
        for i in range(len(strings)):
            if ('(' in strings[i] ) and (')' in strings[i]):
                papers_name.append(strings[i + 2])
                break
    print (papers_name)

    papers_cites_all = []
    papers_cites_other = []


    with open('cite_statics.txt','a') as f:
        for one_name in papers_name:
            query_url = 'https://xs.glgoo.top/scholar?hl=en&as_sdt=0%2C5&q=' + one_name.lstrip().replace(' ', '+').replace(':', "%3A") + '&btnG='
            cite_num_all, cites_url, authors = get_citation(query_url)


            # time.sleep(random.uniform(3,10))
            if cite_num_all != 0:
                self_cite_num = select_self_cite(cite_num_all, cites_url, authors)
            else:
                self_cite_num = 0
            papers_cites_all.append(cite_num_all)
            papers_cites_other.append(cite_num_all - self_cite_num)

            f.write(one_name + ":  google cites all : "  + str(cite_num_all) + " other cites : " + str(cite_num_all - self_cite_num) + '\n')
            print('\n\n\n')
            


    

