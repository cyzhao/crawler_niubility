from urllib import request as ureq
import requests
from lxml import html
import gzip
import re,math
import time,random

url = 'https://xs.glgoo.top/scholar?start=0&cites=12126817180959752761&as_sdt=2005&sciodt=0,5&hl=en'

header_dict={'Host': 'xs.glgoo.top',
             'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.92 Mobile Safari/537.36',
             'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
             'Accept-Language': 'zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3',
             'Referer': 'https://xs.glgoo.top/schhp?hl=zh-CN',
             'Connection': 'keep-alive',
             'cookie':'CONSENT=YES+CN.zh-CN+20180318-09-1; GSP=LM=1533191995:S=vJmdnVriU4rPvp3a; HSID=Aoc6Se-3hEkAAnwkr; SSID=A-a1jEsNw-_3hE2c6; APISID=u4MChJbypLJFHJIj/ASCtQFk8bySjYzRhg; SAPISID=yoqKuGskdFmgQMBN/AmleMiEYN52GcUYv4; SID=zQYRThMe6eQZRVfNMIhLvfLKv78shc3K77oVc2ENTLAIoF5LUwkHz4sgH4IyFntj9NZG1Q.; OGPC=19007661-2:19008186-1:19008076-2:19008374-2:19008966-1:19009193-2:577841152-2:19009353-2:19009950-2:; NID=152=s1lStBnIRtqBYHkee7-6LpGz99iWYijVW07FPltVhgE8hkqlKS04XUW7C1lXuTDep5Kf7OdaM2vR7oI6T3Tc5b_rON0g8A7ZQ1UCfBMPB2GMc15ZN-cjjezweqBSHwbycEmRUSQy566-weXI3oVb01vlKdtE216xrI4qFuNJ2SMzwIhPfNMqHy9zCLJcAKMtLpGpl94qi13CS02VYK0AsuZZX6AUqKNtWuJ7GLZWOFSS; 1P_JAR=2019-1-2-10; SIDCC=ABtHo-HUgz4V9IJJUFa6PDfVglW6t4vrsGjGS1Pn8Q3ElLMNPaqGxYcHmQIWzl8BhKBTDtWDCqo'}
page = requests.Session().get(url,headers=header_dict)

tree = html.fromstring(page.text)

cite_authors = tree.xpath('//div[@id="gs_res_ccl_mid"]')
help(cite_authors)

